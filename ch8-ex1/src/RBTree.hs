{-# LANGUAGE BangPatterns       #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE GADTs              #-}
{-# LANGUAGE InstanceSigs       #-}
{-# LANGUAGE StandaloneDeriving #-}

module RBTree where

import Color
import Set

import Data.Bits ((.&.))
import Data.List (foldl')

import Debug.Trace

data RBTree a where
    E :: RBTree a
    RBTree :: Color -> !(RBTree a) -> Bool -> a -> !(RBTree a) -> RBTree a

data CountedTree t a =
    Counted Int
            Int
            (t a)

lbalance :: Show a => RBTree a -> RBTree a
lbalance (RBTree B (RBTree R (RBTree R a px x b) py y c) pz z d) = rebalanced
  where
    rebalanced = RBTree R (RBTree B a px x b) py y (RBTree B c pz z d)
lbalance (RBTree B (RBTree R a px x (RBTree R b py y c)) pz z d) = rebalanced
  where
    rebalanced = RBTree R (RBTree B a px x b) py y (RBTree B c pz z d)

rbalance :: Show a => RBTree a -> RBTree a
rbalance (RBTree B a px x (RBTree R (RBTree R b py y c) pz z d)) = rebalanced
  where
    rebalanced = RBTree R (RBTree B a px x b) py y (RBTree B c pz z d)
rbalance t@(RBTree B a px x (RBTree R b py y (RBTree R c pz z d))) = rebalanced
  where
    rebalanced = RBTree R (RBTree B a px x b) py y (RBTree B c pz z d)

instance Set RBTree where
    empty :: RBTree a
    empty = E
    insert :: (Ord a, Show a) => a -> RBTree a -> RBTree a
    insert x t = RBTree B l' py' y' r'
      where
        (RBTree _ l' py' y' r') = insert' x t
        insert' :: (Ord a, Show a) => a -> RBTree a -> RBTree a
        insert' v E = RBTree R E True v E
        insert' x t@(RBTree c l b y r)
            | x < y = lbalance (RBTree c (insert' x l) b y r)
            | y < x = rbalance (RBTree c l b y (insert' x r))
            | otherwise = RBTree c l True y r
    delete :: (Ord a, Show a) => a -> RBTree a -> RBTree a
    delete _ E = error "deleting absent member"
    delete x (RBTree c l p y r)
        | x < y = RBTree c (delete x l) p y r
        | x > y = RBTree c l p y (delete x r)
        | otherwise =
            if p
                then RBTree c l False y r
                else error "deleting an already deleted element"
    member :: Ord a => a -> RBTree a -> Bool
    member _ E = False
    member x (RBTree _ l b y r)
        | x < y = member x l
        | x > y = member x r
        | otherwise = True

instance Set (CountedTree RBTree) where
    empty :: CountedTree RBTree a
    empty = Counted 0 0 empty
    insert ::
           (Ord a, Show a) => a -> CountedTree RBTree a -> CountedTree RBTree a
    insert x (Counted ni nd t) = Counted (ni + 1) nd $ insert x t
    delete ::
           (Ord a, Show a) => a -> CountedTree RBTree a -> CountedTree RBTree a
    delete x (Counted ni nd t) = rebuild $ Counted ni (nd + 1) $ delete x t
    member :: Ord a => a -> CountedTree RBTree a -> Bool
    member _ (Counted _ _ E) = False
    member x (Counted _ _ t) = member x t

deriving instance Show a => Show (RBTree a)

deriving instance
         (Show a, Show (RBTree a)) => Show (CountedTree RBTree a)

inOrderTraversal :: RBTree a -> [a]
inOrderTraversal E = []
inOrderTraversal (RBTree _ l p x r) =
    let ls = inOrderTraversal l
        rs = inOrderTraversal r
    in if p then ls ++ [x] ++ rs
            else ls ++ rs

fromList :: (Ord a, Show a) => [a] -> RBTree a
fromList = foldl' (flip insert) empty

log2Ceil = go 0
  where
    go :: Int -> Int -> Int
    go l 1  = l + 1
    go !l n = go (l + 1) (n `div` 2)

isPowerOf2 :: Int -> Bool
isPowerOf2 n = n .&. (n - 1) == 0

formsCompleteTree :: Int -> Bool
formsCompleteTree n = isPowerOf2 (n + 1)

newtype BH =
    BH Int

newtype Count =
    Count Int

fromOrdList :: (Ord a) => Int -> [a] -> RBTree a
fromOrdList n xs = fst $ construct (Count n) (BH (h - 1)) xs
  where
    h = log2Ceil n
    construct :: Count -> BH -> [a] -> (RBTree a, [a])
    construct (Count 0) _ xs = (E, xs)
    construct (Count 1) (BH 1) xs = (RBTree B E True (head xs) E, tail xs)
    construct (Count 1) (BH 0) xs = (RBTree R E True (head xs) E, tail xs)
    construct (Count n) (BH h) xs = (RBTree B l True x r, xsRemaining)
      where
        nL = n `div` 2
        nR = n - nL - 1
        (l, xsAfterL) = construct (Count nL) (BH (h - 1)) xs
        x = head xsAfterL
        (r, xsRemaining) = construct (Count nR) (BH (h - 1)) (tail xsAfterL)

rebuild :: (Ord a) => CountedTree RBTree a -> CountedTree RBTree a
rebuild c@(Counted ni nd t)
  | ni > 0 && ni < 2 * nd =
      let n = ni - nd
      in Counted n 0 $ fromOrdList n $ inOrderTraversal t
  | otherwise   = c
--rebuild = fromOrdList . inOrderTraversal
