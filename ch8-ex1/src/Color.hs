module Color (Color(..)) where

data Color = R | B deriving (Eq, Show)
