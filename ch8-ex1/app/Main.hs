module Main where

import Criterion.Main
import RBTree
import Set

main :: IO ()
main = let l = Counted 10 0 $ fromOrdList 10 [1..10]
           l1 = delete 3 l
           l2 = delete 5 l1
           l3 = delete 1 l2
           l4 = delete 7 l3
           l5 = delete 9 l4
           l6 = delete 2 l5
       in print l6

