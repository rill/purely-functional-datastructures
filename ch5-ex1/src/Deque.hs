{-# LANGUAGE InstanceSigs #-}
module Deque where

import Data.Tuple (swap)

class Deque d where
    empty :: d a
    isEmpty :: d a -> Bool
    cons :: a -> d a -> d a
    head :: d a -> a
    tail :: d a -> d a
    snoc :: d a -> a -> d a
    last :: d a -> a
    init :: d a -> d a

newtype DequeImpl a =
    D ([a], [a]) deriving Show

check :: ([a], [a]) -> ([a], [a])
check ([], [])  = ([], [])
check ([], [x]) = ([], [x])
check ([x], []) = ([x], [])
check ([], xs)  = splitHalf xs
check (xs, [])  = (swap . splitHalf) xs
check p         = p

splitHalf :: [a] -> ([a], [a])
splitHalf xs = (f, r)
  where
    (r, r') = splitAt (length xs `div` 2) xs
    f = reverse r'

instance Deque DequeImpl where
    empty :: DequeImpl a
    empty = D ([], [])
    isEmpty :: DequeImpl a -> Bool
    isEmpty (D ([], [])) = True
    isEmpty _            = False
    cons :: a -> DequeImpl a -> DequeImpl a
    cons x (D (f, r)) = D (x : f, r)
    head :: DequeImpl a -> a
    head d
        | isEmpty d = error "head: empty deque"
    head (D ([], [x])) = x
    head (D (x:xs, _)) = x
    tail :: DequeImpl a -> DequeImpl a
    tail d
        | isEmpty d = error "tail: empty deque"
    tail (D ([], [x])) = empty
    tail (D (x:xs, r)) = D $ check (xs, r)
    snoc :: DequeImpl a -> a -> DequeImpl a
    snoc (D (f, r)) x = D (f, x : r)

    last :: DequeImpl a -> a
    last d
        | isEmpty d = error "last: empty deque"
    last (D ([x], [])) = x
    last (D (_, x:xs)) = x

    init :: DequeImpl a -> DequeImpl a
    init d
        | isEmpty d = error "init: empty deque"
    init (D ([x], [])) = empty
    init (D (f, x:xs)) = D $ check (f, xs)
