module Main where

import Deque

e :: DequeImpl Int
e = empty

main :: IO ()
main = print $ Deque.tail $ Deque.tail (snoc (snoc (cons 1 e) 2) 3)
