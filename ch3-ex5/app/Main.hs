module Main where

import BinomialHeap

main :: IO ()
main = print h
  where
    h = fromList [1 .. 7] :: BinomialHeap Int
