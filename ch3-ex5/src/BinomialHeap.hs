{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs               #-}

module BinomialHeap
    ( BinomialHeap(..)
    , fromList
    ) where

import BinomialTree
import Heap

import Data.List (foldl', minimumBy)
import Data.Ord  (comparing)

newtype Rank =
    Rank Int
    deriving (Eq, Ord, Num, Show)

newtype BinomialHeap a =
    BH [(Rank, Tree a)]
    deriving (Show)

instance Heap BinomialHeap where
    empty :: Ord a => BinomialHeap a
    empty = BH []

    isEmpty :: Ord a => BinomialHeap a -> Bool
    isEmpty (BH []) = True
    isEmpty _       = False

    insert :: Ord a => a -> BinomialHeap a -> BinomialHeap a
    insert v = merge (BH [(Rank 0, BT v [])])

    merge :: Ord a => BinomialHeap a -> BinomialHeap a -> BinomialHeap a
    merge (BH rt1s) (BH rt2s) = BH (combine rt1s rt2s)
      where
        combine :: [(Rank, Tree a)]
                -> [(Rank, Tree a)]
                -> [(Rank, Tree a)]
        combine xs [] = xs
        combine [] xs = xs
        combine (x@(rx, tx):xs) (y@(ry, ty):ys)
            | rx < ry = x : combine xs (y : ys)
            | ry < rx = y : combine (x : xs) ys
            | otherwise =
                insertTree (rx + Rank 1, link tx ty) (combine xs ys)
          where
            insertTree :: (Rank, Tree a)
                       -> [(Rank, Tree a)]
                       -> [(Rank, Tree a)]
            insertTree x [] = [x]
            insertTree x@(rx, tx) (y@(ry, ty):ys)
                | rx < ry = x : y : ys
                | otherwise = insertTree (rx + Rank 1, link tx ty) ys

    findMin :: Ord a => BinomialHeap a -> a
    findMin (BH []) = error "empty heap"
    findMin (BH [(_, t)]) = root t
    findMin (BH ((_, t):ts)) = foldl' (\m (_, t) -> min m (root t)) (root t) ts

    deleteMin :: (Eq a, Ord a) => BinomialHeap a -> BinomialHeap a
    deleteMin h@(BH ts) = merge h' childHeap
      where
        ((Rank r', t'), h') = removeMinTree ts
        cs' = children t'
        childHeap = createHeapFromTrees (reverse cs')
        createHeapFromTrees :: [Tree a] -> BinomialHeap a
        createHeapFromTrees xs = BH $ zip (Rank <$> [1 ..]) xs

removeMinTree :: Ord a => [(Rank, Tree a)] -> ((Rank, Tree a), BinomialHeap a)
removeMinTree [] = error "removing min-tree from empty tree"
removeMinTree [x] = (x, BH [])
removeMinTree (t@(_, x):ts) =
    let (t'@(_, x'), BH ts') = removeMinTree ts
    in if root x < root x'
           then (t, BH ts)
           else (t', BH (t : ts'))

fromList :: Ord a => [a] -> BinomialHeap a
fromList = foldl' (flip insert) empty
