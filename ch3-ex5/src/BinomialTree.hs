{-# LANGUAGE GADTs              #-}
{-# LANGUAGE StandaloneDeriving #-}

module BinomialTree
    ( Tree(..)
    , children
    , root
    , link
    ) where

data Tree a where
    BT :: Ord a => a -> [Tree a] -> Tree a
deriving instance Show a => Show (Tree a)

link :: Tree a -> Tree a -> Tree a
link t1@(BT v1 c1s) t2@(BT v2 c2s)
    -- it is assumed that t1 and t1 have the same rank
    | v1 < v2 = BT v1 (t2 : c1s)
    | otherwise = BT v2 (t1 : c2s)

root :: Tree a -> a
root (BT v _) = v

children :: Tree a -> [Tree a]
children (BT _ cs) = cs
