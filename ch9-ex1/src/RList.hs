module RList where

data RListSubTree a
    = Leaf a
    | Node Int
           (RListSubTree a)
           (RListSubTree a)
    deriving Show

data Digit a = Zero | One (RListSubTree a) deriving Show
type RList a = [Digit a]

drop :: Int -> RList a -> RList a
drop 0 rlist = rlist
drop _ []    = []
drop k rlist = dropInitial k [] rlist
  where
      dropInitial:: Int -> RList a -> RList a -> RList a
      dropInitial 0 zs rlist     =  zs ++ rlist
      dropInitial k zs (Zero:ds) = dropInitial k (Zero : zs) ds
      dropInitial k zs (One t@(Node size _ _):ds)
        | k == size = zs ++ (Zero : ds)
        | k > size = dropInitial (k - size) (Zero : zs) ds
        | otherwise = dropWithin k t ++ (Zero : ds)
      dropWithin :: Int -> RListSubTree a -> RList a
      dropWithin _ (Leaf _) = error "called dropWithin for a leaf"
      dropWithin _ (Node 2 (Leaf l) (Leaf r)) = [One (Leaf r)]
      dropWithin k (Node size l r) =
          let halfSize = size `div` 2
          in if k == halfSize
                then zeroes r ++ [One r]
                else if k > halfSize
                    then dropWithin (k - halfSize) r ++ [Zero]
                    else dropWithin k l ++ [One r]
      zeroes (Node _ (Leaf _) (Leaf _)) = [Zero]
      zeroes (Node _ l _)               = Zero : zeroes l


{-
 -(!) :: RList a -> Int -> a
 -rlist ! n = findWithinTree t i
 -  where
 -    (t, i) = findTree rlist n
 -    findTree = go 1
 -      where
 -        go treeSize (t:ts) index
 -            | index < treeSize = (t, index)
 -            | otherwise = go (2 * treeSize) ts (index - treeSize)
 -    findWithinTree (Leaf v) _ = v
 -    findWithinTree (Node l r) index
 -        | even index = findWithinTree l (index `div` 2)
 -        | otherwise = findWithinTree r (index `div` 2)
 -
 -}
