module Main where

import Criterion.Main

insertionSort :: (Ord a) => [a] -> [a]
insertionSort xs = x : insertionSort xs'
  where
    (x, xs') = findmin xs
    findmin :: (Ord a) => [a] -> (a, [a])
    findmin (x:xs) =
        let go :: Ord a => a -> [a] -> [a] -> (a, [a])
            go x rs [] = (x, rs)
            go x rs (y:ys)
                | x < y = go x (y : rs) ys
                | otherwise = go y (x : rs) ys
        in go x [] xs

--[1000,999..1] = [1000, 999 .. 1] :: [Int]
--[10000,9999..1] = [10000, 9999 .. 1] :: [Int]
--[100000,99999..1] = [100000,99999 .. 1] :: [Int]

main :: IO ()
main =
    defaultMain
        [ bgroup
              "10^3"
              [ bench "5" $
                nf (take 5 . insertionSort) ([1000,999..1] :: [Int])
              , bench "50" $
                nf (take 50 . insertionSort) ([1000,999..1] :: [Int])
              , bench "500" $
                nf (take 500 . insertionSort) ([1000,999..1] :: [Int])
              ]
        , bgroup
              "10^4"
              [ bench "5" $
                nf (take 5 . insertionSort) ([10000,9999..1] :: [Int])
              , bench "50" $
                nf (take 50 . insertionSort) ([10000,9999..1] :: [Int])
              , bench "500" $
                nf (take 500 . insertionSort) ([10000,9999..1] :: [Int])
              ]
        , bgroup
              "10^5"
              [ bench "5" $
                nf (take 5 . insertionSort) ([100000,99999..1] :: [Int])
              , bench "50" $
                nf (take 50 . insertionSort) ([100000,99999..1] :: [Int])
              , bench "500" $
                nf (take 500 . insertionSort) ([100000,99999..1] :: [Int])
              ]
        ]
