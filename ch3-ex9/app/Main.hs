module Main where

import Criterion.Main
import RBTree

main :: IO ()
main =
    --print $ fromOrdList 10 [1..10]
    defaultMain
        [ bgroup
              "fromOrdList"
              [ --bench "10" $ whnf fromOrdList [1..10]
              --, bench "100" $ whnf (fromOrdList 100) [1..100]
              --,
              bench "10^3" $ whnf (fromOrdList (10^3)) [1..10^3]
              --,
              , bench "10^4" $ whnf (fromOrdList (10^4)) [1..10^4]
              , bench "10^5" $ whnf (fromOrdList (10^5)) [1..10^5]
              , bench "10^6" $ whnf (fromOrdList (10^6)) [1..10^6]
              , bench "10^7" $ whnf (fromOrdList (10^7)) [1..10^7]
              ]
        ]
