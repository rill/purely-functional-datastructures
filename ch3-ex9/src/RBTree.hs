{-# LANGUAGE BangPatterns       #-}
{-# LANGUAGE GADTs              #-}
{-# LANGUAGE InstanceSigs       #-}
{-# LANGUAGE StandaloneDeriving #-}

module RBTree where

import Color
import Set

import Data.Bits ((.&.))
import Data.List (foldl')

import Debug.Trace

data RBTree a where
    E :: RBTree a
    RBTree :: Color -> !(RBTree a) -> a -> !(RBTree a) -> RBTree a

lbalance :: Show a => RBTree a -> RBTree a
lbalance (RBTree B (RBTree R (RBTree R a x b) y c) z d) = rebalanced
  where
    rebalanced = RBTree R (RBTree B a x b) y (RBTree B c z d)
lbalance (RBTree B (RBTree R a x (RBTree R b y c)) z d) = rebalanced
  where
    rebalanced = RBTree R (RBTree B a x b) y (RBTree B c z d)
rbalance :: Show a => RBTree a -> RBTree a
rbalance (RBTree B a x (RBTree R (RBTree R b y c) z d)) = rebalanced
  where
    rebalanced = RBTree R (RBTree B a x b) y (RBTree B c z d)
rbalance t@(RBTree B a x (RBTree R b y (RBTree R c z d))) = rebalanced
  where
    rebalanced = RBTree R (RBTree B a x b) y (RBTree B c z d)
balance t = t

instance Set RBTree where
    empty :: RBTree a
    empty = E
    insert :: (Ord a, Show a) => a -> RBTree a -> RBTree a
    insert x t = RBTree B l' y' r'
      where
        (RBTree _ l' y' r') = insert' x t
        insert' :: (Ord a, Show a) => a -> RBTree a -> RBTree a
        insert' v E = RBTree R E v E
        insert' x t@(RBTree c l y r)
            | x < y = lbalance (RBTree c (insert' x l) y r)
            | y < x = rbalance (RBTree c l y (insert' x r))
            | otherwise = t
    member :: Ord a => a -> RBTree a -> Bool
    member _ E = False
    member x (RBTree _ l y r)
        | x < y = member x l
        | x > y = member x r
        | otherwise = True

deriving instance Show a => Show (RBTree a)

fromList :: (Ord a, Show a) => [a] -> RBTree a
fromList = foldl' (flip insert) empty

log2Ceil = go 0
  where
    go :: Int -> Int -> Int
    go l 1  = l + 1
    go !l n = go (l + 1) (n `div` 2)

isPowerOf2 :: Int -> Bool
isPowerOf2 n = n .&. (n - 1) == 0

formsCompleteTree :: Int -> Bool
formsCompleteTree n = isPowerOf2 (n + 1)

newtype BH =
    BH Int

newtype Count =
    Count Int

fromOrdList :: (Ord a, Show a) => Int -> [a] -> RBTree a
fromOrdList n xs = fst $ construct (Count n) (BH (h - 1)) xs
  where
    h = log2Ceil n
    construct :: Count -> BH -> [a] -> (RBTree a, [a])
    construct (Count 0) _ xs = (E, xs)
    construct (Count 1) (BH 1) xs = (RBTree B E (head xs) E, tail xs)
    construct (Count 1) (BH 0) xs = (RBTree R E (head xs) E, tail xs)
    construct (Count n) (BH h) xs = (RBTree B l x r, xsRemaining)
      where
        nL = n `div` 2
        nR = n - nL - 1
        (l, xsAfterL) = construct (Count nL) (BH (h - 1)) xs
        x = head xsAfterL
        (r, xsRemaining) =
            construct (Count nR) (BH (h - 1)) (tail xsAfterL)
