module Main where

data Digit = One | Two deriving Show
type Nat = [Digit]

inc :: Nat -> Nat
inc []       = [One]
inc (One:ds) = Two : ds
inc (Two:ds) = One : inc ds

dec :: Nat -> Nat
dec [One]    = []
dec [Two]    = [One]
dec (One:ds) = Two : dec ds
dec (Two:ds) = One : ds

add xs []             = xs
add [] xs             = xs
add (One:xs) (One:ys) = Two : add xs ys
add (Two:xs) (One:ys) = One : inc (add xs ys)
add (One:xs) (Two:ys) = One : inc (add xs ys)
add (Two:xs) (Two:ys) = Two : inc (add xs ys)

main :: IO ()
main = print $ take 8 $ iterate inc []
