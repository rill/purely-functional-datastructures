{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs               #-}

module LeftistHeap
    ( LeftistHeap
    , fromList
    ) where

import Data.List (foldl')
import Heap

newtype Rank =
    Rank Int
    deriving (Num, Eq, Ord, Show)

data LeftistHeap a
    = E
    | T !Rank
        a
        !(LeftistHeap a)
        !(LeftistHeap a)
    deriving (Show)

rank :: LeftistHeap a -> Rank
rank E           = Rank 0
rank (T r _ _ _) = r

makeT :: a -> LeftistHeap a -> LeftistHeap a -> LeftistHeap a
makeT x a b
    | rank b <= rank a = T (rank b + Rank 1) x a b
    | otherwise = T (rank a + Rank 1) x b a

instance Heap LeftistHeap where
    empty = E
    isEmpty E = True
    isEmpty _ = False
    insert v E = T (Rank 1) v E E
    insert v h@(T _ x l r)
        | v < x = T (Rank 1) v h E
        | otherwise = makeT x l (insert v r)
    merge h E = h
    merge E h = h
    merge h1@(T rank1 v1 l1 r1) h2@(T rank2 v2 l2 r2)
        | v1 < v2 = makeT v1 l1 (merge r1 h2)
        | otherwise = makeT v2 l2 (merge h1 r2)
    findMin (T _ v _ _) = v
    deleteMin (T _ _ l r) = merge l r

instance Ord a => Semigroup (LeftistHeap a) where
    (<>) = merge

instance Ord a => Monoid (LeftistHeap a) where
    mempty = empty
    mconcat xs =
        case concatPairs xs of
            [x]    -> x
            halfXs -> mconcat halfXs
      where
        concatPairs []           = []
        concatPairs [x]          = [x]
        concatPairs (x1:x2:rest) = x1 <> x2 : concatPairs rest

fromList :: Ord a => [a] -> LeftistHeap a
fromList xs = mconcat heaps
  where
    heaps = fmap (`insert` E) xs
