module Main where

import LeftistHeap

main :: IO ()
main = print $ fromList ([1..16*1024] :: [Int])
