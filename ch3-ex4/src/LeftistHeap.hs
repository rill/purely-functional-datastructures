{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs               #-}

module LeftistHeap
    ( LeftistHeap
    , fromList
    ) where

import Data.List (foldl')
import Heap

newtype Weight =
    Weight Int
    deriving (Num, Eq, Ord, Show)

data LeftistHeap a
    = E
    | T !Weight
        a
        !(LeftistHeap a)
        !(LeftistHeap a)
    deriving (Show)

weight :: LeftistHeap a -> Weight
weight E           = Weight 0
weight (T w _ _ _) = w

instance Heap LeftistHeap where
    empty = E
    isEmpty E = True
    isEmpty _ = False
    insert v E = T (Weight 1) v E E
    insert v h@(T _ x l r)
        | v < x = T w v h E
        | otherwise =
            let wl = weight l
                wr = weight r + Weight 1
            in if wl >= wr
                   then T w x l (insert v r)
                   else T w x (insert v r) l
        where w = weight h + Weight 1
    merge h E = h
    merge E h = h
    merge h1@(T _ v1 l1 r1) h2@(T _ v2 l2 r2)
        | v1 < v2 =
            let wl = weight l1
                wr = weight r1 + weight h2
                w = wl + wr + Weight 1
            in if wl >= wr
                then T w v1 l1 (merge r1 h2)
                else T w v1 (merge r1 h2) l1
        | otherwise =
            let wl = weight l2
                wr = weight r2 + weight h1
                w = wl + wr + Weight 1
            in if wl >= wr
                then T w v2 l2 (merge r2 h1)
                else T w v2 (merge r2 h1) l2
    findMin (T _ v _ _) = v
    deleteMin (T _ _ l r) = merge l r

instance Ord a => Semigroup (LeftistHeap a) where
    (<>) = merge

instance Ord a => Monoid (LeftistHeap a) where
    mempty = empty
    mconcat xs =
        case concatPairs xs of
            [x]    -> x
            halfXs -> mconcat halfXs
      where
        concatPairs []           = []
        concatPairs [x]          = [x]
        concatPairs (x1:x2:rest) = x1 <> x2 : concatPairs rest

fromList :: Ord a => [a] -> LeftistHeap a
fromList xs = mconcat heaps
  where
    heaps = fmap (`insert` E) xs
