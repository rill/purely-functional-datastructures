module Main where

import LeftistHeap

main :: IO ()
main = print $ fromList ([1..10] :: [Int])
