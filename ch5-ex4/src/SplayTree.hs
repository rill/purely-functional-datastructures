module SplayTree where

data Tree a
    = E
    | T (Tree a)
        a
        (Tree a)
    deriving (Show)

smaller :: (Ord a) => a -> Tree a -> Tree a
smaller _ E = E
smaller pivot (T a1 y1 b1)
    | pivot < y1 = smaller pivot a1
    | otherwise =
        case b1 of
            E -> T a1 y1 E
            T a2 y2 b2 ->
                if pivot < y2
                    then T a1 y1 (smaller pivot a2)
                    else T (T a1 y1 a2) y2 (smaller pivot b2)

bigger :: (Ord a) => a -> Tree a -> Tree a
bigger _ E = E
bigger pivot (T a1 y1 b1)
    | pivot >= y1 = bigger pivot b1
    | otherwise =
        case a1 of
            E -> T E y1 b1
            T a2 y2 b2 ->
                if pivot >= y2
                    then T (bigger pivot b2) y1 b1
                    else T (bigger pivot a2) y2 (T b2 y1 b1)

partition :: (Ord a) => a -> Tree a -> (Tree a, Tree a)
partition _ E = (E, E)
partition x (T a1 y1 b1)
    | x < y1 =
        case a1 of
            E -> (E, T E y1 b1)
            T a2 y2 b2 ->
                if x < y2
                    then let pa2 = partition x a2
                         in (fst pa2, T (snd pa2) y2 (T b2 y1 b1))
                    else let pb2 = partition x b2
                         in (T a2 y2 (fst pb2), T (snd pb2) y1 b1)
    | otherwise =
        case b1 of
            E -> (T a1 y1 E, E)
            T a2 y2 b2 ->
                if x < y2
                    then let pa2 = partition x a2
                         in (T a1 y1 (fst pa2), T (snd pa2) y2 b2)
                    else let pb2 = partition x b2
                         in (T (T a1 y1 a2) y2 (fst pb2), snd pb2)

insert :: (Ord a) => a -> Tree a -> Tree a
insert x t = let pt = partition x t in T (fst pt) x (snd pt)
