module Main where

import Criterion.Main

data SpineStrictList a
    = Nil
    | Cons a
           !(SpineStrictList a)
    deriving (Show)

{-sublists :: [Int] -> SpineStrictList [Int]-}
{-sublists [x] = Cons [x] Nil-}
{-sublists xs  = Cons xs $ sublists (tail xs)-}

sublists :: [Int] -> [[Int]]
sublists [x] = [[x]]
sublists xs  = xs : sublists (tail xs)

len :: List a -> Int
len Nil = 0
len (Cons _ rest) = go 1 rest
  where
    go n Nil = n
    go n (Cons _ xs) =
        let n' = n + 1
        in go n' xs

main :: IO ()
main =
    defaultMain
        [ bench "10" $ whnf length $ sublists [1 .. 10]
        , bench "100" $ whnf length $ sublists [1 .. 100]
        , bench "1000" $ whnf length $ sublists [1 .. 1000]
        ]
{-main = print $ length $ sublists [1 .. 10 ^ 7]-}
